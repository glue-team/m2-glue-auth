<?php


declare(strict_types=1);

namespace GlueAgency\BackendGoogleSignOn\Model;

use Google_Client;
use Google_Service_Oauth2;

class GoogleServiceOauthFactory
{
    /**
     * Create a new Google_Service_Oauth2 instance
     *
     * @param Google_Client $client
     * @return Google_Service_Oauth2
     */
    public function create(Google_Client $client): Google_Service_Oauth2
    {
        return new Google_Service_Oauth2($client);
    }
}
