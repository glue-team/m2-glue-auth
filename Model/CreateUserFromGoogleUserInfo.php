<?php


declare(strict_types=1);

namespace GlueAgency\BackendGoogleSignOn\Model;

use Google_Service_Oauth2_Userinfo;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\User\Model\ResourceModel\User as UserResource;
use Magento\User\Model\UserFactory;
use GlueAgency\BackendGoogleSignOn\Model\ResourceModel\GetUsernameByEmail;

class CreateUserFromGoogleUserInfo
{
    /**
     * @var UserFactory
     */
    private $userFactory;

    /**
     * @var UserResource
     */
    private $userResource;

    /**
     * @var GetDomainRule
     */
    private $getDomainRule;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var GetUsernameByEmail
     */
    private $getUsernameByEmail;

    /**
     * @param GetDomainRule $getDomainRule
     * @param GetUsernameByEmail $getUsernameByEmail
     * @param UserFactory $userFactory
     * @param UserResource $userResource
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        GetDomainRule $getDomainRule,
        GetUsernameByEmail $getUsernameByEmail,
        UserFactory $userFactory,
        UserResource $userResource
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->userFactory = $userFactory;
        $this->userResource = $userResource;
        $this->getDomainRule = $getDomainRule;
        $this->getUsernameByEmail = $getUsernameByEmail;
    }

    /**
     * Return true if user already exists
     *
     * @param string $email
     * @return bool
     */
    private function userExistsByEmail(string $email): bool
    {
        try {
            $this->getUsernameByEmail->execute($email);
            return true;
        } catch (NoSuchEntityException $e) {
            return false;
        }
    }

    private function getConnection()
    {
        return $this->resourceConnection->getConnection();
    }

    private function updatePwdExpiryDate($savedUserEmail)
    {
        try{
            $userData = $this->getConnection()->fetchRow(
                $this->getConnection()
                    ->select()
                    ->from('admin_user')
                    ->where('email = :email')
                    ->limit(1),
                [':email' => $savedUserEmail]
            );
            $userId = $userData['user_id'];
        }catch (\Throwable $exception){
            $msg = $exception->getMessage();
            $userId = 0;
        }

        $this->getConnection()->update(
            'admin_passwords',
            [
                'last_updated' => '1878426000',//Wednesday, July 11, 2029 1:00:00 AM
            ],
            [
                'user_id = ?' => (int)$userId
            ]
        );
    }

    /**
     * Create a new admin user if not exists based on google info
     *
     * @param Google_Service_Oauth2_Userinfo $userInfo
     * @throws AlreadyExistsException
     */
    public function execute(Google_Service_Oauth2_Userinfo $userInfo): void
    {
        $rule = $this->getDomainRule->execute($userInfo->getHd());

        if (!is_numeric($rule) || $this->userExistsByEmail($userInfo->getEmail())) {
            return;
        }

        $user = $this->userFactory->create();
        $user->setFirstname($userInfo->getGivenName() ?: 'Google');
        $user->setLastname($userInfo->getFamilyName() ?: 'User');
        $user->setUsername($userInfo->getEmail());
        $user->setPassword(md5(uniqid((string) time(), true)));
        $user->setEmail($userInfo->getEmail());
        $user->setInterfaceLocale($userInfo->getLocale());
        $user->setRoleId($rule);
        $user->setIsActive(1);
        $this->userResource->save($user);
        $this->updatePwdExpiryDate($user->getEmail());
    }

    public function executeWithDataArray(array $userInfo): void
    {
        $rule = $this->getDomainRule->execute($userInfo['hd']);

        if (!is_numeric($rule) || $this->userExistsByEmail($userInfo['email'])) {
            return;
        }

        $user = $this->userFactory->create();
        $user->setFirstname($userInfo['given_name'] ?: 'Google');
        $user->setLastname($userInfo['family_name'] ?: 'User');
        $user->setUsername($userInfo['email']);
        $user->setPassword(md5(uniqid((string) time(), true)));
        $user->setEmail($userInfo['email']);
        $user->setInterfaceLocale('en_US');
        $user->setRoleId($rule);
        $user->setIsActive(1);
        $this->userResource->save($user);
        $this->updatePwdExpiryDate($user->getEmail());
    }    
}
