<?php


declare(strict_types=1);

namespace GlueAgency\BackendGoogleSignOn\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class IsEnabled
{
    /**
     * XML path to enabled flag
     */
    private const XML_PATH_ENABLED = 'glueagency_backend_google_sign_on/general/enabled';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Return true if the module is enabled
     *
     * @return bool
     */
    public function execute(): bool
    {
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_ENABLED);
    }
}
