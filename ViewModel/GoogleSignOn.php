<?php


declare(strict_types=1);

namespace GlueAgency\BackendGoogleSignOn\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Backend\Helper\Data;
use GlueAgency\BackendGoogleSignOn\Model\GetGoogleClient;

class GoogleSignOn implements ArgumentInterface
{
    /**
     * @var GetGoogleClient
     */
    private $getGoogleClient;

    private DeploymentConfig $deploymentConfig;

    private \Magento\Backend\Helper\Data $backendHelper;

    /**
     * @param GetGoogleClient $getGoogleClient
     */
    public function __construct(
        GetGoogleClient $getGoogleClient,
        DeploymentConfig $deploymentConfig,
        \Magento\Backend\Helper\Data $backendHelper
    ) {
        $this->getGoogleClient = $getGoogleClient;
        $this->deploymentConfig = $deploymentConfig;
        $this->backendHelper = $backendHelper;
    }

    /**
     * Get google auth URL
     *
     * @return string
     */
    public function getAuthUrl(): string
    {
        $url = $this->deploymentConfig->get('google_client_callback_url') . '?' . http_build_query([
            'site' => $this->backendHelper->getHomePageUrl().'index/index',
        ]);

        return $url;

        // // $client = $this->getGoogleClient->execute();
        // // return $client->createAuthUrl();
    }
}
