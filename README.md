# Backend Google Sign On for Magento 2
This is a Magento 2 extension that allows to authenticate into Magento 2 Admin Panel using Google Sign On services.

## Installation
Install the extension through composer:

    composer require glue-agency/m2-glue-auth
    
Once Composer finishes installing the dependencies, enable the module:

    php bin/magento module:enable GlueAgency_BackendGoogleSignOn

Finally, run the upgrade:

    php bin/magento setup:upgrade

## Configuration
After the module is installed and enabled, authenticate into Admin Panel and go to `STORES > Settings > Configuration > SECURITY > Google Backend Sign On`

Here you can turn the module on.

You can also define a set of domain-based rules to automatically assign a role to a user or to force a user to use the Google Sign On.

For example: `example.com => create user in Administrators`

You also need to set 2 env variables in app/etc/env.php

    'google_client_callback_url' => '',//to be set
    'glue_encryption_token' => '',//to be set

The callback url may most likely default to: `https://oauth.glue.be/oauth/callback`

The token you need to retrieve from other projects or bitwarden.

## Usage
Once enabled and configured, the extension adds a **GLUE Login** button in the sign in form of the Admin Panel.

You can authenticate with your Google account and the magento2 admin user will be created in the database and will be logged in automatically.