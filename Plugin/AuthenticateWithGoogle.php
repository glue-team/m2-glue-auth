<?php


declare(strict_types=1);

namespace GlueAgency\BackendGoogleSignOn\Plugin;

use Exception;
use Magento\Backend\Controller\Adminhtml\Auth\Login;
use Magento\Backend\Model\Auth\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Event\ManagerInterface;
use GlueAgency\BackendGoogleSignOn\Model\AuthenticateByEmail;
use GlueAgency\BackendGoogleSignOn\Model\CreateUserFromGoogleUserInfo;
use GlueAgency\BackendGoogleSignOn\Model\GetGoogleUserInfo;
use GlueAgency\BackendGoogleSignOn\Model\IsEnabled;

class AuthenticateWithGoogle
{
    /**
     * @var GetGoogleUserInfo
     */
    private $getGoogleUserInfo;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var AuthenticateByEmail
     */
    private $authenticateByEmail;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var Session
     */
    private $authSession;

    /**
     * @var IsEnabled
     */
    private $isEnabled;

    private DeploymentConfig $deploymentConfig;

    /**
     * @var CreateUserFromGoogleUserInfo
     */
    private $createUserFromGoogleUserInfo;

    /**
     * @param RequestInterface $request
     * @param IsEnabled $isEnabled
     * @param GetGoogleUserInfo $getGoogleUserInfo
     * @param AuthenticateByEmail $authenticateByEmail
     * @param Session $authSession
     * @param ManagerInterface $eventManager
     * @param CreateUserFromGoogleUserInfo $createUserFromGoogleUserInfo
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        RequestInterface $request,
        IsEnabled $isEnabled,
        GetGoogleUserInfo $getGoogleUserInfo,
        AuthenticateByEmail $authenticateByEmail,
        Session $authSession,
        ManagerInterface $eventManager,
        DeploymentConfig $deploymentConfig,
        CreateUserFromGoogleUserInfo $createUserFromGoogleUserInfo,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->getGoogleUserInfo = $getGoogleUserInfo;
        $this->request = $request;
        $this->authenticateByEmail = $authenticateByEmail;
        $this->eventManager = $eventManager;
        $this->messageManager = $messageManager;
        $this->authSession = $authSession;
        $this->isEnabled = $isEnabled;
        $this->deploymentConfig = $deploymentConfig;
        $this->createUserFromGoogleUserInfo = $createUserFromGoogleUserInfo;
    }

    /**
     * @param Login $subject
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeExecute(Login $subject): void
    {
        if (!$this->isEnabled->execute()) {
            return;
        }

        $data = $this->request->getParam('data', '');
        if (empty($data)) {
            return;
        }

        try {
            if(! $this->deploymentConfig->get('glue_encryption_token')){
                throw new \Exception('login module configuration is incomplete');
            }

            $encrypter = new \Illuminate\Encryption\Encrypter(base64_decode($this->deploymentConfig->get('glue_encryption_token')),'aes-256-cbc');

            $data = $encrypter->decrypt($this->request->getParam('data', ''));

            $this->createUserFromGoogleUserInfo->executeWithDataArray($data);

            $this->authenticateByEmail->execute($data['email']);

            $this->eventManager->dispatch(
                'backend_auth_user_login_success',
                ['user' => $this->authSession]
            );
        } catch (Exception $e) {
            $this->eventManager->dispatch(
                'backend_auth_user_login_failed',
                ['user_name' => '', 'exception' => $e]
            );

            $this->messageManager->addErrorMessage($e->getMessage());
        }
    }
}
